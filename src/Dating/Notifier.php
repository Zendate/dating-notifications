<?php
namespace Dating;
// ini_set("display_errors",1);
// error_reporting(E_ALL);

use OW_Event;
use OW;

class Notifier
{
    /**
     * Sphinx client
     * @var SphinxClient
     */
    private $sphinx;

    /**
     * Redis client
     * @var Credis_Client
     */
    private $redis;

    /**
     * Notifier instance
     * @var \Dating\Notifier
     */
    private static $instance;

    private $sendLogic = [
        'wink'      => [ 'morning', 'evening', 'user_datetime_tomorrow_10', 'user_datetime_tomorrow_18' ],
        'view'      => [ 'morning', 'evening', 'user_datetime_tomorrow_10', 'user_datetime_tomorrow_18' ],
        'fav'       => [ 'midday', 'evening', 'user_datetime_tomorrow_14', 'user_datetime_tomorrow_18' ],
        'message'   => [ 'morning', 'midday', 'evening', 'user_datetime_tomorrow_10', 'user_datetime_tomorrow_14', 'user_datetime_tomorrow_18' ],
        'zen_like'  => [ 'midday', 'user_datetime_tomorrow_14' ]
    ];


    private $start_local_cron_time = [
        'speedmatch'=> [14, 50],
        'view'      => [15, 08],
        'fav'       => [15, 28],
        'wink'      => [15, 48],
        'fake_msg'  => [16, 08],
        ];

    /**
     * Private constructor to avoid creation of multiple instances
     */
    private function __construct()
    {
        //require_once _DIR__.'../../../sphinx-2.2.9-release/api/sphinxapi.php';
        $this->sphinx = new \SphinxClient("localhost", 9312);
        if ($sphinx_error = $this->sphinx->GetLastError())
            echo sprintf("Notifier __construct sphinx error: %s\n", $sphinx_error);
        $this->redis = new \Credis_Client('127.0.0.1', 6379);
    }

    /**
     * Creates Notifier instance
     * @return \Dating\Notifier
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Adds user to notification tracker
     * @param integer $userId User id
     * @param string $matchAge Formatted string f.e. "18-20"
     * @param integer $userGender Gender 1 - male, 2 - female
     */
    public function addUser($userId, $matchAge, $userGender)
    {
        /*

        !!! look function resetCounters !!!

        $this->redis->zadd('wink_user', $userId, $userId); // Users list for Winks
        $this->redis->zadd('fav_user', $userId, $userId);  // Users list for Favorite
        $this->redis->zadd('msg_user', $userId, $userId);  // Users list for Messages
        $this->redis->zadd('speedmatch_user', $userId, $userId);  // Users list for SpeedMatch
        //*/
        //*
        $userId = (int)$userId;

        $this->redis->zadd('last_new_users'. (OW_DEV_MODE ? '_dev' : '') , $userId, $userId);

        $matches = $this->findMatches($matchAge, $userGender == 1 ? 2 : 1);

        foreach ($matches as $uid => $info) {
            $this->redis->zadd('wink_matches'. (OW_DEV_MODE ? '_dev:' : ':') . $userId, $uid, $uid); // Matches for Winks
            $this->redis->zadd('fav_matches' . (OW_DEV_MODE ? '_dev:' : ':') . $userId, $uid, $uid);  // Matches for Favorite
            $this->redis->zadd('msg_matches' . (OW_DEV_MODE ? '_dev:' : ':') . $userId, $uid, $uid);  // Matches for Messages
            $this->redis->zadd('speedmatch_matches' . (OW_DEV_MODE ? '_dev:' : ':') . $userId, $uid, $uid); // Matches for SpeedMatch
        }

        //$this->sendWinks( [$userId], true );
        //$this->sendMessage( [$userId], true );  // view
        $this->sendFakeMsg( [$userId], true ); // message
        //$this->addToFavorite( [$userId], true );
        //$this->sendSpeedMatch( [$userId], true );
        //*/
    }

    /**
     * Finds matches for given $age and $gender
     * @param string $age Formatted string f.e. "18-20"
     * @param integer $gender // 1 - male, 2 - femaile
     */
    private function findMatches($age, $gender)
    {
        $this->sphinx->setLimits(0, 2000, 2000); //1k users
        $this->sphinx->setFilter('sex', array($gender));
        if ( !$age )
            $age = '26-35';
        $parts = explode('-', $age);

        $min = new \DateTime('now');
        $max = clone $min;
        $min = $min->sub(new \DateInterval("P{$parts[0]}Y"))->format('Y');
        $max = $max->sub(new \DateInterval("P{$parts[1]}Y"))->format('Y');

        $this->sphinx->setFilterRange('birthdate', $max+2, $min-2);
        //$result = $this->sphinx->query("", "IMPORTED");
        $result = $this->sphinx->query("", "USERCOUPLE");

        if ($result && $result['total_found'] > 0) {
            return $result['matches'];
        }

        return array();
    }

    /**
     * Sends winks to user
     * @param array $users
     */
    public function sendWinks(array $users = [], $is_now = false)
    {
        if (empty($users) )
            echo "=========== start:".date('Y-m-d H:i:s')."=========\n";
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('wink_user' . (OW_DEV_MODE ? '_dev' : ''), 0, -1));

        $service = \WINKS_BOL_Service::getInstance();
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $i = 0;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);

        foreach ($notify as $uid) {
            $uid = $uid;
            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 28*60, 40*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $key == 'now' && $is_now ) {}
                else {
                    if ( !in_array($key, $this->sendLogic['wink'] ) ) continue;
                }

                if ( $pushTime == null) continue;

                $count_key = 'winks_sent:'.$uid;
                if ( !$is_now )
                {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $count = $this->redis->get($count_key);
                        if ( $count >= 3 ) continue 2; // 3 winks alredy sent for today
                    }
                }

                $viewerId = $this->redis->zrange('wink_matches' . (OW_DEV_MODE ? '_dev:' : ':') . $uid, 0, 1);

                if (is_array($viewerId)) $viewerId = $viewerId[0];

                if ($viewerId == 0 || $viewerId === null) { 
                    echo 'continue user id'.PHP_EOL;
                    continue 2;
                }

                // Push notification time
                $pushTime->setTimezone(new \DateTimeZone('UTC'));
                $mandrill_time = clone $pushTime;
                $service->sendWink($viewerId, $uid, true, $pushTime, true);

                $this->redis->zrem('wink_matches' . (OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('fav_matches' . (OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('msg_matches'. (OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                if (empty($users) )
                    echo sprintf("%d sends wink to %d\n", $viewerId, $uid);

                if ( !$is_now ){
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $this->redis->incr($count_key);
                    }
                }

                $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' =>  sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s winked at you. Wink back while %s online!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she'),
                            //'link_url' => 'https://bnc.lt/m/eArjJ1qeyp'
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_winked_me', $user->api_version),
                        ],
                        "Somebody has winked at you", $mandrill_time);
                    return sprintf("Just now %s winked at you. Wink back while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she');
                }, "WINK", $viewerId, $pushTime, false);
            }
            $i++;
        }
        if (empty($users) )
            echo "sent count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
    }
    /**
     * Sends add to favourite notifications
     * @param array $users
     */
    public function addToFavorite(array $users = null, $is_now = false)
    {
        if (empty($users) )
            echo "=========== start:".date('Y-m-d H:i:s')."=========\n";
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('fav_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1));

        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $i = 0;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);
        foreach ($notify as $uid)
        {
            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 40*60, 50*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $key == 'now' && $is_now ) {} 
                else {
                    if ( !in_array($key, $this->sendLogic['fav'] ) ) continue;
                }

                if ( $pushTime == null ) continue;
                $count_key = 'fav_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

                if ( !$is_now )
                {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $count = $this->redis->get($count_key);
                        if ( $count >= 3 ) continue 2; // Limit 3 fav alredy send today
                    }
                }
                $viewerId = $this->redis->zrange('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, 0, 1);
             
                if (is_array($viewerId)) $viewerId = $viewerId[0];
             
                if ($viewerId == 0 || $viewerId === null) continue 2;
                /*
                if ( $viewerId == 743373 ) {
                    $this->redis->zrem('wink_matches:' . $uid, $viewerId);
                    $this->redis->zrem('fav_matches:' . $uid, $viewerId);
                    $this->redis->zrem('msg_matches:' . $uid, $viewerId);
                    $this->redis->zrem('speedmatch_matches:'.$uid, $viewerId);

                    $viewerId = $this->redis->zrange('fav_matches:'.$uid, 0, 1);

                    if (is_array($viewerId)) $viewerId = $viewerId[0];

                    if ($viewerId == 0 || $viewerId === null) { 
                        continue 2;
                    }
                }
                //*/
                $pushTime->setTimezone(new \DateTimeZone('UTC')); // convert user timesone to UTC time zone
                $mandrill_time = clone $pushTime;

                \OW::getEventManager()->call("bookmarks.mark", array(
                    "userId"         => $viewerId,
                    "markUserId"     => $uid,
                    "timeStampVisit" => $mandrill_time->getTimestamp(),
                    "isautomark"     => true,
                    "iscron"         => true
                ));
             
                $this->redis->zrem('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                $this->redis->zrem('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                $this->redis->zrem('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                if (empty($users) )
                    echo sprintf("%d fav to %d\n", $viewerId, $uid);

                if ( !$is_now ) {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $this->redis->incr($count_key);
                    }
                }

                $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' =>  sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s added you to Favorites. Send %s a message while %s online!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she'),
                            //'link_url' => 'https://bnc.lt/m/sy7xzbJeyp'
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_faved_me', $user->api_version),
                        ],
                        "Somebody added you to favorites", $mandrill_time);
             
                    return sprintf("Just now %s added you to Favorites. Send %s a message while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she');
                }, "FAVED", $viewerId, $pushTime, false);
            }
            $i++;
        }
        if (empty($users) )
            echo "sent count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
    }
    /**
     * View
     */
    public function sendMessage(array $users = null, $is_now = false)
    {
        if ( empty($users) )
            echo "=========== start:".date('Y-m-d H:i:s')."=========\n";
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('msg_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1));
        $service = \WINKS_BOL_Service::getInstance();
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $i = 0;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);
        foreach ($notify as $uid)
        {
            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 35*60, 50*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $key == 'now' && $is_now ) {}
                else {
                    if ( !in_array($key, $this->sendLogic['view'] ) ) continue;
                }
                if ( $pushTime == null ) continue;

                $count_key = 'msg_sent'.(OW_DEV_MODE ? '_dev:' : ':') . $uid;

                if ( !$is_now )
                {
                    $count = $this->redis->get($count_key);
                    if ( $count >= 3 ) continue 2; // Limit 3 view alredy send today
                }

                $viewerId = $this->redis->zrange('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, 0, 1);
                if ( is_array($viewerId) ) $viewerId = $viewerId[0];

                if ( $viewerId == 0 || $viewerId === null ) continue 2;
                
                $pushTime->setTimezone(new \DateTimeZone('UTC')); // convert user timesone to UTC time zone
                $mandrill_time = clone $pushTime;

                $event = new \OW_Event('guests.track_visit', array(
                    'userId' => $uid,
                    'guestId' => $viewerId,
                    'isautovisit' => true,
                    'timeStampVisit' => $mandrill_time->getTimestamp(),
                    "iscron" => true
                ));
                OW::getEventManager()->trigger($event);

                $this->redis->zrem('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                if ( empty($users) )
                    echo sprintf("%d sends msg to %d\n", $viewerId, $uid);

                if ( !$is_now ) {
                    $this->redis->incr($count_key);
                }

                $parse->sendPush($uid, function ( $sender, $user ) use ( $parse, $mandrill_time ) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>%s checked you out! Find out more.</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8'))),
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_viewed_me', $user->api_version),
                        ],
                        "Somebody viewed your profile", $mandrill_time);

                    return sprintf("%s checked you out! Find out more.", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                }, "VIEW", $viewerId, $pushTime, false);
            }
            $i++;
        }
        if (empty($users) )
            echo "sent count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
    }

    public function test()
    {
    }

    public function resetCounters()
    {
        $yesterday_users_key = 'last_users'.(OW_DEV_MODE ? '_dev_' : '_').date('Y_m_d', strtotime('-1 days'));

        $yesterday_new_users = $this->redis->zrange($yesterday_users_key, 0, -1);

        foreach ( $yesterday_new_users as $uid ) {
            $this->redis->zadd('wink_user'.(OW_DEV_MODE ? '_dev' : ''), $uid, $uid); // Users list for Winks
            $this->redis->zadd('fav_user'.(OW_DEV_MODE ? '_dev' : ''), $uid, $uid);  // Users list for Favorite
            $this->redis->zadd('msg_user'.(OW_DEV_MODE ? '_dev' : ''), $uid, $uid);  // Users list for Messages
            $this->redis->zadd('speedmatch_user'.(OW_DEV_MODE ? '_dev' : ''), $uid, $uid);  // Users list for SpeedMatch
        }

        $this->redis->del( $yesterday_users_key );

        $winkUsers = $this->redis->zrange('wink_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);

        foreach ($winkUsers as $uid) {
            $count_key = 'winks_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

            if ($this->redis->get($count_key) >= 2) {
                $this->redis->del($count_key);
            }
        }

        $favUsers = $this->redis->zrange('fav_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);

        foreach ($favUsers as $uid) {
            $count_key = 'fav_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

            if ($this->redis->get($count_key) >= 2) {
                $this->redis->del($count_key);
            }
        }

        $favUsers = $this->redis->zrange('msg_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);

        foreach ($favUsers as $uid) {
            $count_key = 'msg_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

            if ($this->redis->get($count_key) >= 2) {
                $this->redis->del($count_key);
            }
            
            $count_key = 'msg_sent_text'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

            if ($this->redis->get($count_key) >= 2) {
                $this->redis->del($count_key);
            }
        }

        $favUsers = $this->redis->zrange('speedmatch_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);

        foreach ($favUsers as $uid) {
            $count_key = 'speedmatch_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

            if ($this->redis->get($count_key) >= 2) {
                $this->redis->del($count_key);
            }
        }
        //*/
    }

    public function removeAll() {
        /*
        $notify = $this->redis->zrange('wink_user', 0, -1);
        foreach ($notify as $uid) {
            echo $uid.PHP_EOL;
            $this->redis->zrem('wink_user', $uid);
            $this->redis->zrem('fav_user', $uid);
            $this->redis->zrem('msg_user', $uid);
        }
        //*/
    }

    public function getUserUTCFuturePushTimes($user_time_zone = 'UTC', $is_today = false, $is_now = false, $first_time = 0, $last_time = 3600)
    {
        $user_datetime = new \DateTime('now', new \DateTimeZone($user_time_zone));

        $user_datetime_10 = clone $user_datetime;
        $user_datetime_14 = clone $user_datetime;
        $user_datetime_18 = clone $user_datetime;
        $user_datetime_24 = clone $user_datetime;

        $user_datetime_tomorrow_10 = clone $user_datetime;
        $user_datetime_tomorrow_14 = clone $user_datetime;
        $user_datetime_tomorrow_18 = clone $user_datetime;

        $interval_10 = mt_rand(0, 14400);
        $interval_14 = mt_rand(0, 14400);
        $interval_18 = mt_rand(0, 14400);

        $user_datetime_10->setTime(10, 0, 0);
        $user_datetime_14->setTime(14, 0, 0);
        $user_datetime_18->setTime(18, 0, 0);
        $user_datetime_24->setTime(24, 0, 0);

        $user_datetime_tomorrow_10->setTime(10, 0, 0);
        $user_datetime_tomorrow_14->setTime(14, 0, 0);
        $user_datetime_tomorrow_18->setTime(18, 0, 0);

        $user_datetime_tomorrow_10->add(new \DateInterval('P1D'));
        $user_datetime_tomorrow_14->add(new \DateInterval('P1D'));
        $user_datetime_tomorrow_18->add(new \DateInterval('P1D'));

        $return_date_time = [];

        if ( $is_today )
        {
            if ( $is_now )
            {
                $interval_now = mt_rand($first_time, $last_time);
                $user_datetime_now = new \DateTime();
                $user_datetime_now->add(new \DateInterval('PT' . $interval_now . 'S'));
                $return_date_time['now'] = $user_datetime_now;
                return $return_date_time;
            }
            else
            {
                if ( $user_datetime < $user_datetime_10)
                {
                    $user_datetime_10->add(new \DateInterval('PT'.$interval_10.'S'));
                    $user_datetime_14->add(new \DateInterval('PT'.$interval_14.'S'));
                    $user_datetime_18->add(new \DateInterval('PT'.$interval_18.'S'));

                    $return_date_time['morning'] = $user_datetime_10;
                    $return_date_time['midday'] = $user_datetime_14;
                    $return_date_time['evening'] = $user_datetime_18;
                }
                else if ( $user_datetime >= $user_datetime_10 && $user_datetime <= $user_datetime_14 )
                {
                    $user_datetime_14->add(new \DateInterval('PT'.$interval_14.'S'));
                    $user_datetime_18->add(new \DateInterval('PT'.$interval_18.'S'));
                    
                    $return_date_time['midday'] = $user_datetime_14;
                    $return_date_time['evening'] = $user_datetime_18;
                }
                else if ( $user_datetime >= $user_datetime_14 && $user_datetime <= $user_datetime_18 )
                {
                    $user_datetime_18->add(new \DateInterval('PT'.$interval_18.'S'));
                    $return_date_time['evening'] = $user_datetime_18;
                }

                if ($user_datetime >= $user_datetime_14 && $user_datetime <= $user_datetime_24)
                {
                    $interval_10 = mt_rand(0, 14400);
                    $interval_14 = mt_rand(0, 14400);
                    $interval_18 = mt_rand(0, 14400);
                    $user_datetime_tomorrow_10->add(new \DateInterval('PT'.$interval_10.'S'));
                    $user_datetime_tomorrow_14->add(new \DateInterval('PT'.$interval_14.'S'));
                    $user_datetime_tomorrow_18->add(new \DateInterval('PT'.$interval_18.'S'));

                    $return_date_time['user_datetime_tomorrow_10'] = $user_datetime_tomorrow_10;
                    $return_date_time['user_datetime_tomorrow_14'] = $user_datetime_tomorrow_14;
                    $return_date_time['user_datetime_tomorrow_18'] = $user_datetime_tomorrow_18;
                }
            }
        }
        else
        {
            if ( $user_datetime >= $user_datetime_10 && $user_datetime <= $user_datetime_14 )
            {
                $user_datetime_10->add(new \DateInterval('P1D'));
            }
            else if ( $user_datetime >= $user_datetime_14 && $user_datetime <= $user_datetime_18 )
            {
                $user_datetime_10->add(new \DateInterval('P1D'));
                $user_datetime_14->add(new \DateInterval('P1D'));
            }
            else if ( $user_datetime >= $user_datetime_18 && $user_datetime <= $user_datetime_24 )
            {
                $user_datetime_10->add(new \DateInterval('P1D'));
                $user_datetime_14->add(new \DateInterval('P1D'));
                $user_datetime_18->add(new \DateInterval('P1D'));
            }

            $user_datetime_10->add(new \DateInterval('PT'.$interval_10.'S'));
            $user_datetime_14->add(new \DateInterval('PT'.$interval_14.'S'));
            $user_datetime_18->add(new \DateInterval('PT'.$interval_18.'S'));
            
            $return_date_time['morning'] = $user_datetime_10;
            $return_date_time['midday']  = $user_datetime_14;
            $return_date_time['evening'] = $user_datetime_18;
        }

        return $return_date_time;
    }

    public function removeUsersFromRedis()
    {
        $winkUsers = $this->redis->zrange('wink_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);
        $favUsers = $this->redis->zrange('fav_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);
        $msgUsers = $this->redis->zrange('msg_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);
        $speedMatchUsers = $this->redis->zrange('speedmatch_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);
        
        $userIds = \BOL_UserService::getInstance()->getUsersRegistersLastMonths(array_merge($winkUsers, $favUsers, $msgUsers, $speedMatchUsers));
        echo '--- start '.date('Y-m-d') . '---' . PHP_EOL;

        foreach ($userIds as $userId)
        {
            try {
                $this->redis->del('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':').$userId); // Matches for Winks
                $this->redis->del('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':'). $userId);  // Matches for Favorite
                $this->redis->del('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':'). $userId);  // Matches for Messages
                $this->redis->del('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':'). $userId);  // Matches for SpeedMatch
            
                $this->redis->zrem('wink_user'.(OW_DEV_MODE ? '_dev' : ''), $userId); // Matches for Winks
                $this->redis->zrem('fav_user'.(OW_DEV_MODE ? '_dev' : ''), $userId);  // Matches for Favorite
                $this->redis->zrem('msg_user'.(OW_DEV_MODE ? '_dev' : ''), $userId);  // Matches for Messages
                $this->redis->zrem('speedmatch_user'.(OW_DEV_MODE ? '_dev' : ''), $userId);  // Matches for SpeedMatch

                $count_key = 'fav_sent'.(OW_DEV_MODE ? '_dev:' : ':').$userId;
                $this->redis->del($count_key);

                $count_key = 'msg_sent'.(OW_DEV_MODE ? '_dev:' : ':').$userId;
                $this->redis->del($count_key);
                
                $count_key = 'msg_sent_text'.(OW_DEV_MODE ? '_dev:' : ':').$userId;
                $this->redis->del($count_key);
                
                $count_key = 'speedmatch_sent'.(OW_DEV_MODE ? '_dev:' : ':').$userId;
                $this->redis->del($count_key);

                //$this->redis->zrem('new_users_with_token', $userId);
            }
            catch (Exception $ex)
            {
                echo $ex->getMessage().PHP_EOL;
                continue;
            }
        }
        echo 'remove ' . count($userIds) . ' count' . PHP_EOL;
        echo '--- end '.date('Y-m-d') . '---' . PHP_EOL;
    }

    public function addLostUsers()
    {
        $lostUsers = \BOL_UserService::getInstance()->getLostUsers();

        //*/
        foreach($lostUsers as $id => $userData)
        {
            $this->addUser($id, $userData['match_age'], $userData['accountType']);
        }//*/
    }
    /**
     * Sends SpeedMatch message notifications Zenmatch
     * @param array $users
     */
    public function sendSpeedMatch(array $users = null, $is_now = false)
    {
        try {
        //echo "=========== start:".date('Y-m-d H:i:s')."=========\n";
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('speedmatch_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1));
        //$service = \WINKS_BOL_Service::getInstance();
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $service = \SKADATE_BOL_Service::getInstance();
        $i = 0;

        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);

        foreach ($notify as $uid) {
            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 50*60, 60*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $key == 'now' && $is_now ) {}
                else {
                    if ( !in_array($key, $this->sendLogic['zen_like'] ) ) continue;
                }
                if ( $pushTime == null ) continue;

                $count_key = 'speedmatch_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;

                if ( !$is_now )
                {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $count = $this->redis->get($count_key);
                        if ( $count >= 3 ) continue 2; // 3 winks alredy sent for today
                    }
                }

                $viewerId = $this->redis->zrange('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, 0, 1);

                if (is_array($viewerId)) $viewerId = $viewerId[0];

                if ($viewerId == 0 || $viewerId === null) continue 2;
                
                // Push notification time
                $pushTime->setTimezone(new \DateTimeZone('UTC')); // convert user timesone to UTC time zone
                $mandrill_time = clone $pushTime;
                //$mandrill_time->setTimezone(new \DateTimeZone('UTC')); // convert user timesone to UTC time zone
                // send speedmatch
                $service->addSpeedmatchRelation($viewerId, $uid, 1, $pushTime->getTimestamp(), true, true);

                $this->redis->zrem('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                if (empty($users) )
                    echo sprintf("%d sends speedmatch to %d\n", $viewerId, $uid);

                if ( !$is_now ) {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $this->redis->incr($count_key);
                    }
                }
                $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>
                            Just now %s liked your profile in %s. %s is online now. Don`t miss it!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')),(int)$user->api_version == 1 ? 'ZenMatch' : 'QuickMatch', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
                            //'link_url' => 'https://bnc.lt/m/eArjJ1qeyp'
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_faved_me', $user->api_version),
                        ],
                       sprintf("Somebody liked your profile in %s", (int)$user->api_version == 1 ? 'ZenMatch' : 'QuickMatch'), $mandrill_time);
                    return sprintf("Just now %s liked your profile in %s. %s is online now. Don`t miss it!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), (int)$user->api_version == 1 ? 'ZenMatch' : 'QuickMatch', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                }, "FAVED", $viewerId, $pushTime, false);
            }
            $i++;
        }

        }
        catch(Exception $ex) {
            echo $ex->getMessage();
        }
        if (empty($users) )
            echo "sent speedmatch count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
    }
    /**
     * Sends SpeedMatch message notifications
     * @param array $users
     */
    public function sendFakeMsg(array $users = null, $is_now = false)
    {
        try {
        if (empty($users) )
            echo "=========== send fake msg text start:".date('Y-m-d H:i:s')."=========\n";
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('msg_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1));
        $service = \WINKS_BOL_Service::getInstance();
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $i = 0;
        //$uid    = 708014;

        //$notify = [$uid];
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);
        foreach ($notify as $uid) {

            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            //$pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 14*60, 27*60 );
            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 35*60, 50*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $key == 'now' && $is_now ) {}
                else {
                    if ( !in_array($key, $this->sendLogic['message'] ) ) continue;
                }
                if ( $pushTime == null ) continue;

                $count_key = 'msg_sent_text'.(OW_DEV_MODE ? '_dev:' : ':') . $uid;

                if ( !$is_now ) 
                {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $count = $this->redis->get($count_key);
                        if ( $count >= 3 ) continue 2; // Limit 3 max send today
                    }
                } // 3 winks alredy sent for today
             
                $viewerId = $this->redis->zrange('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, 0, 1);
                if (is_array($viewerId)) $viewerId = $viewerId[0];
             
                if ($viewerId == 0 || $viewerId === null) continue 2;
                
                $pushTime->setTimezone(new \DateTimeZone('UTC'));
                $mandrill_time = clone $pushTime;
             
                $message_template_id = null;
                $userId              = $viewerId;
                $opponentId          = $uid;
                $text                = null;

                $idMessageTemplateWasSend   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplateIdsFromMessageBySenderAndRecipient($opponentId);
                $aMessageTemplateType1or2   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplatesByTypeIgnoreIds(array(1, 2), $idMessageTemplateWasSend);
                $messTemplateIndex          = mt_rand(0, count($aMessageTemplateType1or2)-1);
                $templateSelected           = $aMessageTemplateType1or2[$messTemplateIndex];

                $this->redis->zrem('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $uid, $viewerId);
                $this->redis->zrem('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                $this->redis->zrem('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);
                $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':').$uid, $viewerId);

                if ($templateSelected === null) continue;

                $text                = $templateSelected->text;
                $message_template_id = $templateSelected->id;

                $is_send_view = mt_rand(0, 1);

                if ( $is_send_view)
                {
                    if (empty($users) )
                        echo 'send view before sending fake msg'.PHP_EOL;
                    $event = new \OW_Event('guests.track_visit', array(
                        'userId'         => $uid,
                        'guestId'        => $viewerId,
                        'isautovisit'    => true,
                        'timeStampVisit' => $mandrill_time->getTimestamp(),
                        "iscron"         => true
                    ));
                    \OW::getEventManager()->trigger($event);
                    
                    $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                        $parse->sendEmail($user->email, $sender,
                            [
                                'user_name' => ucfirst(mb_strtolower($user->username)),
                                'sender_name' => ucfirst(mb_strtolower($sender->username)),
                                'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8'))),
                                'link_url' => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version), 

                            ],
                            "Somebody viewed your profile", $mandrill_time);
                        //return sprintf("%s has viewed your profile.", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                        return sprintf("%s checked you out! Find out more", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                        //return sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                    }, "VIEW", $viewerId, $pushTime, false);

                    $interval_time = mt_rand(30, 120);

                    $mandrill_time->add(new \DateInterval('PT'.$interval_time.'S'));
                    $pushTime->add(new \DateInterval('PT'.$interval_time.'S'));
                }

                \OW::getEventManager()->call('mailbox.post_message',
                    array(
                        'mode'                  => 'chat',
                        'userId'                => $userId,
                        'opponentId'            => $opponentId,
                        'text'                  => $text,
                        'is_cron'               => true,
                        'is_auto'               => true,
                        'message_template_id'   => $message_template_id,
                        'timeStamp'             => $mandrill_time->getTimestamp(),
                    )
                );

                $idMessageTemplateWasSend[] = $message_template_id;

                $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name'     => ucfirst(mb_strtolower($user->username)),
                            'sender_name'   => ucfirst(mb_strtolower($sender->username)),
                            'message' =>  sprintf("<h2 style='font-weight:normal !important;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
                            //'link_url'      => 'https://bnc.lt/m/DizNpFWdyp',
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version),
                            'link_title'    => 'Read Message'
                        ],
                        "Somebody wrote you a new message.", $mandrill_time);
                        return sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                }, "message", $viewerId, $pushTime, false);


                if ( !$is_now ) {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $this->redis->incr($count_key);
                    }
                }

                // ============== send second message for message type = 1 ============================
                if ($templateSelected->type == 1)
                {
                    // select continue of message
                    $aMessageTemplateType3          = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplatesByTypeIgnoreIds(array(3), $idMessageTemplateWasSend);
                    if ( !empty($aMessageTemplateType3) )
                    {
                        $messTemplateIndex3     = mt_rand(0, count($aMessageTemplateType3)-1);
                        $templateSelected3      = $aMessageTemplateType3[$messTemplateIndex3];

                        if ( $templateSelected3 == null ) continue;

                        $interval_time = mt_rand(30, 60);

                        $mandrill_time->add(new \DateInterval('PT'.$interval_time.'S'));
                        $pushTime->add(new \DateInterval('PT'.$interval_time.'S'));

                        $message_template_id    = $templateSelected3->id;
                        $text                   = $templateSelected3->text;

                        OW::getEventManager()->call('mailbox.post_message',
                            array(
                                'mode'                  => 'chat',
                                'userId'                => $userId,
                                'opponentId'            => $opponentId,
                                'text'                  => $text,
                                'is_cron'               => true,
                                'is_auto'               => true,
                                'message_template_id'   => $message_template_id,
                                'timeStamp'             => $mandrill_time->getTimestamp(),
                            )
                        );

                        $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                            $parse->sendEmail($user->email, $sender,
                                [
                                    'user_name'     => ucfirst(mb_strtolower($user->username)),
                                    'sender_name'   => ucfirst(mb_strtolower($sender->username)),
                                    'message' =>  sprintf("<h2 style='font-weight:normal !important;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
                                    'link_url' => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version), //'link_url'      => 'https://bnc.lt/m/DizNpFWdyp',
                                    'link_title'    => 'Read Message'
                                ],
                                "Somebody wrote you a new message.", $mandrill_time);
                                return sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                        }, "message", $viewerId, $pushTime, false);
                    }
                }
                // ============== end send second message for message type = 1 ============================
                if ( empty($users))
                    echo sprintf("%d sends msg text to %d\n", $viewerId, $uid);
            }
            $i++;
        }

        if (empty($users) )
            echo "sent msg text count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
        }
        catch (Exception $ex)
        {
            echo $ex->getMessage() . PHP_EOL;
        }
    }

    public function last_new_users(array $users = [] )
    {
        echo 'last new users start ' . date('Y-m-d H:i:s') . PHP_EOL;
        $notify = $users ? $users : $this->redis->zrange('last_new_users'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);

        if ( count($notify) == 0) {
            echo 'last users count is 0' . PHP_EOL;
            return;
        }

        if ( empty($users) )
        {
            echo 'remove from last day' . PHP_EOL;
            $last_users_key = 'last_users'.(OW_DEV_MODE ? '_dev_' : '_').date('Y_m_d');
            foreach ($notify as $uid)
            {
                echo 'del ' . $uid . PHP_EOL;
                $this->redis->zadd($last_users_key, $uid, $uid);
                $this->redis->zrem('last_new_users'.(OW_DEV_MODE ? '_dev' : ''), $uid);
            }
            echo 'end remove from last day' .date('Y-m-d H:i:s') . PHP_EOL;
        }
        $send_is_now = false;
        $this->sendWinks( $notify, $send_is_now );
        $this->sendMessage( $notify, $send_is_now );  // view
        $this->sendSpeedMatch( $notify, $send_is_now );
        $this->sendFakeMsg( $notify, $send_is_now ); // message
        $this->addToFavorite( $notify, $send_is_now );
        echo 'last new users stop ' . date('Y-m-d H:i:s') . PHP_EOL;
    }

    public function new_users_with_token()
    {
        echo 'new users with token'.PHP_EOL;
        //*
        $users = $this->redis->zrange('new_users_with_token'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);

        if ( count($users) == 0 ) return;

        //*
        foreach ($users as $uid) {
            $this->redis->zrem('new_users_with_token'.(OW_DEV_MODE ? '_dev' : ''), $uid);
            echo 'zrem '.$uid;
        }

        $pushTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        //*/
        echo count($users).PHP_EOL;
        //*
        foreach($users as $userId)
        {
            // --- Wink ---
            $winkSrv = \WINKS_BOL_Service::getInstance();
            $winks = $winkSrv->getFeatureSend($userId);
            // --- FAV ---
            $favSrv = \BOOKMARKS_BOL_Service::getInstance();
            $favs  = $favSrv->getFeatureSend($userId);
            // --- View ---
            $viewSrv = \OCSGUESTS_BOL_Service::getInstance();
            $views  = $viewSrv->getFeatureSend($userId);
            // --- SpeedMatch ---
            $speedMatchSrv = \SKADATE_BOL_Service::getInstance();
            $speedMatches = $speedMatchSrv->getFeatureSend($userId);

            $fakeMsgSrv = \MAILBOX_BOL_ConversationService::getInstance();
            $fakeMsgs = $fakeMsgSrv->getFeatureSend($userId);


            echo 'wink' . PHP_EOL;
            echo count($winks) . PHP_EOL;
            foreach ($winks as $wink)
            {
                $pushTime->setTimestamp($wink->timestamp);

                if (time() < $wink->timestamp) {
                    $parse->sendPush(
                        $userId,
                        function($sender, $user, $pushTime) { 
                            //return sprintf("Wow! %s winked at you", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                            return sprintf("Just now %s winked at you. Wink back while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'She', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her');
                        },
                        "WINK", $wink->userId, $pushTime, false
                    );
                }
            }
            echo 'fav' . PHP_EOL;
            echo count($winks) . PHP_EOL;

            foreach ($favs as $fav)
            {
                $pushTime->setTimestamp($fav->timestamp);
                
                if (time() < $fav->timestamp) {
                    $parse->sendPush($userId,
                        function ( $sender, $user, $pushTime)
                        {
                            //return sprintf("%s added you to Favorites.", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                            return sprintf("Just now %s added you to Favorites. Send %s a message while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she');
                        },
                        "FAVED", $fav->userId, $pushTime, false
                    );
                }
            }//*/
            echo 'view' . PHP_EOL;
            echo count($views) . PHP_EOL;
            foreach ($views as $view)
            {
                $pushTime->setTimestamp($view->visitTimestamp);

                if (time() < $view->visitTimestamp) {
                    $parse->sendPush($userId,
                        function ( $sender, $user, $pushTime)
                        {
                            //return sprintf("%s has viewed your profile.", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                            return sprintf("%s checked you out! Find out more .", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                        },
                        "VIEW", $view->guestId, $pushTime, false
                    );
                }
            }
            echo 'speed' . PHP_EOL;
            echo count($speedMatches) . PHP_EOL;
            foreach ($speedMatches as $speedMatch)
            {
                $pushTime->setTimestamp($speedMatch->time_view);

                if (time() < $speedMatch->time_view ) {
                    $parse->sendPush($userId,
                        function ( $sender, $user, $pushTime)
                        {
                            return sprintf("Just now %s liked your profile in %s. %s is online now. Don't miss it!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), (int)$user->api_version == 1 ? 'ZenMatch' : 'QuickMatch', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                        },
                        "FAVED", $speedMatch->userId, $pushTime, false
                    );
                }
            }
            echo 'fakeMsg' . PHP_EOL;
            echo count($fakeMsgs) .PHP_EOL;
            foreach ($fakeMsgs as $fakeMsg)
            {
                $pushTime->setTimestamp($fakeMsg->timeStamp);

                if (time() < $fakeMsg->timeStamp) {
                    $parse->sendPush($userId,
                        function ( $sender, $user, $pushTime)
                        {
                            return sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                        },
                        "message", $fakeMsg->senderId, $pushTime, false
                    );
                }
            }
            echo 'end' . PHP_EOL;
        }
    }

    public function remove_users_with_token($usersId = array())
    {
        foreach($usersId as $uid) {
            $this->redis->zrem('new_users_with_token'.(OW_DEV_MODE ? '_dev' : ''), $uid);
        }
    }

    public function testSendPush() {
        $userId = 742483;

        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $pushTime = new \DateTime('now');
        $pushTime->setTimezone(new \DateTimeZone('UTC'));

        $parse->sendPush(
            $userId,
            function($sender, $user)
            { 
                return sprintf("Wow! %s winked at you test UTC 2 min ago )))", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
            },
            "WINK", $userId
        );
    }
    
    public function sendWinksTest(array $users = [], $is_now = false)
    {
        if (empty($users) )
            echo "=========== start:".date('Y-m-d H:i:s')."=========\n";
        $notify = array(726985); //$users ? $users : array_map('intval', $this->redis->zrange('wink_user', 0, -1));

        try {
            $service = \WINKS_BOL_Service::getInstance();
            $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
            $i = 0;
            $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);
         
            foreach ($notify as $uid) {
                $uid = $uid;
                $user_time_zone = 'America/New_York';
                if (isset($usersTimezone[$uid]))
                {
                    $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
                }
         
                $pushTimes = $this->getUserUTCFuturePushTimesTest($user_time_zone, true, false, 2*60, 5*60 );
                echo $uid.PHP_EOL;
                var_dump($pushTimes);
                foreach($pushTimes as $key => $pushTime)
                {
                    if ( $pushTime == null ) continue;
         
                    $count_key = 'winks_sent'.(OW_DEV_MODE ? '_dev:' : ':').$uid;
                    if ( !$is_now )
                    {
                        if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                            $count = $this->redis->get($count_key);
                            //if ( $count >= 2 ) continue 2; // 3 winks alredy sent for today
                        }
                    }
         
                    //$viewerId = $this->redis->zrange('wink_matches:'.$uid, 0, 1);
                    //if (is_array($viewerId)) $viewerId = $viewerId[0];
                    $viewerId = 712443;
         
                    if ($viewerId == 0 || $viewerId === null) { 
                        echo 'continue user id'.PHP_EOL;
                        continue 2;}
         
                    // Push notification time
                    $pushTime->setTimezone(new \DateTimeZone('UTC'));
                    $mandrill_time = clone $pushTime;
                    //$service->sendWink($viewerId, $uid, true, $pushTime, true);
                    //$this->redis->zrem('wink_matches:' . $uid, $viewerId);
                    //$this->redis->zrem('fav_matches:' . $uid, $viewerId);
                    //$this->redis->zrem('msg_matches:' . $uid, $viewerId);
                    //$this->redis->zrem('speedmatch_matches:'.$uid, $viewerId);
                    if (empty($users) )
                        echo sprintf("%d sends wink to %d\n", $viewerId, $uid);
         
                    if ( !$is_now )
                        $this->redis->incr($count_key);

                    $user = \BOL_UserService::getInstance()->findUserById($uid); 
                    $sender = \BOL_UserService::getInstance()->findUserById($viewerId); 

                    //*
                    $resEmail = $parse->sendEmail($user->email, $sender,
                         array(
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s has winked at you.</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8'))),
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_winked_me', $user->api_version), //'link_url' => 'https://bnc.lt/IAah/mPAaVNssyA'
                         ),
                         "Somebody has winked at you", $mandrill_time);
                    var_dump($resEmail);
                    //*/
         
                    $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                        return sprintf("Wow! %s winked at you. Without email. Age" . $mandrill_time->getTimestamp(), ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                    }, "WINK", $viewerId, $pushTime, false);
                }
                $i++;
            }
            if (empty($users) )
                echo "sent count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
        }
        catch (Exception $ex)
        {
            echo $ex->getMessage();
        }
    }
    
    public function getUserUTCFuturePushTimesTest($user_time_zone = 'UTC')
    {
        $user_datetime = new \DateTime();

        if ( empty($user_time_zone) ) 
            $user_time_zone = 'UTC';

        $user_datetime->getTimezone(new \DateTimeZone($user_time_zone));

        $user_datetime_10 = clone $user_datetime;
        $user_datetime_14 = clone $user_datetime;
        $user_datetime_18 = clone $user_datetime;
        $user_datetime_24 = clone $user_datetime;

        $interval_10 = 180; //mt_rand(120, 14400);
        $interval_14 = 360; //mt_rand(3600, 14400);
        $interval_18 = 540; //mt_rand(3600, 14400);

        $user_datetime_10->add(new \DateInterval('PT'.$interval_10.'S'));
        $user_datetime_14->add(new \DateInterval('PT'.$interval_14.'S'));
        $user_datetime_18->add(new \DateInterval('PT'.$interval_18.'S'));

        return array(
            $user_datetime_10,
            $user_datetime_14,  
            $user_datetime_18,
        );
    }

    public function getFirstMessage($user)
    {
        try {
        $viewerId = $this->redis->zrange('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, 0, 1);

        if (is_array($viewerId)) $viewerId = $viewerId[0];

        if ($viewerId == 0 || $viewerId == null) return;

        $sender = \BOL_UserService::getInstance()->findUserById($viewerId);

        $idMessageTemplateWasSend   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplateIdsFromMessageBySenderAndRecipient($viewerId);
        $aMessageTemplateType1or2   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplatesByTypeIgnoreIds(array(2), $idMessageTemplateWasSend);
        $messTemplateIndex          = mt_rand(0, count($aMessageTemplateType1or2)-1);
        $templateSelected           = $aMessageTemplateType1or2[$messTemplateIndex];

        //$time_view = time() + 30;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone([$user->id]);
        $time_view = new \DateTime('now', new \DateTimeZone('UTC'));
        $time_view->add(new \DateInterval('PT250S'));
        //*
        $user =  \BOL_UserService::getInstance()->findByIdWithoutCache($user->id);

        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $parse->sendEmail($user->email, $sender,
            [
                'user_name'     => ucfirst(mb_strtolower($user->username)),
                'sender_name'   => ucfirst(mb_strtolower($sender->username)),
                'message'       => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
                //'link_url'      => 'https://bnc.lt/m/DizNpFWdyp',
                'link_url'      => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version),
                'link_title'    => 'Read Message'
            ],
            "Somebody wrote you a new message.", $time_view);

        $this->redis->zrem('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, $viewerId);
        $this->redis->zrem('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':').$user->id, $viewerId);
        $this->redis->zrem('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':').$user->id, $viewerId);
        $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':').$user->id, $viewerId);

        $time_view->setTimezone( new \DateTimeZone( $usersTimezone[$user->id] ) );
        \OW::getEventManager()->call('mailbox.post_message',
            array(
                'mode'                  => 'chat',
                'userId'                => $viewerId,
                'opponentId'            => $user->id,
                'text'                  => $templateSelected->text,
                'is_cron'               => true,
                'is_auto'               => true,
                'message_template_id'   => $templateSelected->id,
                'timeStamp'             => $time_view->getTimestamp(),
            )
        );
        //*/
        return [
            'timestamp' => $time_view->getTimestamp(),
            'message'   => sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
            'idUser'    => $viewerId,
            'screen'    => 'MESSAGE'
            ];
        }
        catch (Exception $ex) {
            trigger_error('sendfirstmsg' . $ex->getMessage());
            return [];
        }
    }

    public function getFirstWink($user)
    {
        $viewerId = $this->redis->zrange('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, 0, 1);

        if (is_array($viewerId)) $viewerId = $viewerId[0];

        if ($viewerId == 0 || $viewerId == null) return;

        $sender = \BOL_UserService::getInstance()->findUserById($viewerId);

        //$time_view = time() + 105;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone([$user->id]);
        $time_view = new \DateTime('now', new \DateTimeZone('UTC'));
        $time_view->add(new \DateInterval('PT45S'));
        //*
        $this->redis->zrem('msg_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, $viewerId);
        $this->redis->zrem('fav_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, $viewerId);
        $this->redis->zrem('wink_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, $viewerId);
        $this->redis->zrem('speedmatch_matches'.(OW_DEV_MODE ? '_dev:' : ':') . $user->id, $viewerId);

        //*
        try {
            $service = \WINKS_BOL_Service::getInstance();
            $user =  \BOL_UserService::getInstance()->findByIdWithoutCache($user->id);

            $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
            $parse->sendEmail($user->email, $sender,
            [
                'user_name' => ucfirst(mb_strtolower($user->username)),
                'sender_name' => ucfirst(mb_strtolower($sender->username)),
                'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s winked at you. Wink back while %s online!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she'),
                //'link_url' => 'https://bnc.lt/m/eArjJ1qeyp'
                'link_url' => \OW_Config::getInstance()->getValue('branch_email_winked_me', $user->api_version),
            ],
            "Somebody has winked at you", $time_view);
        } 
        catch (Exception $ex) {
            var_dump($ex->getMessage() . ' errrrrror');
        }
        
        $time_view->setTimezone( new \DateTimeZone($usersTimezone[$user->id]) );
        $service->sendWink($viewerId, $user->id, true, $time_view, true);


        return [
            'timestamp' => $time_view->getTimestamp(),
            'message'   => sprintf("Just now %s winked at you. Wink back while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her'),
            'idUser'    => $viewerId,
            'screen'    => 'WINK'
        ];
        //*/
    }

    public function updateResetBuySubscription()
    {
        \BOL_UserService::getInstance()->updateResetBuySubscription();
    }

        public function fakeMsgBack()
    {
        $userIds = \BOL_UserService::getInstance()->getUserIdsForFakeMsgBack();
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();

        \BOL_UserService::getInstance()->updateTimeStampResetCounter($userIds, time() + mt_rand(3*60, 5*60));

        foreach ($userIds as $userId)
        {
            $usersReceiveFakeMsgKey     = 'usersReceiveFakeMsg'.(OW_DEV_MODE ? '_dev:' : ':') . $userId;
            $usersReceiveFakeMsg        = $this->redis->sMembers($usersReceiveFakeMsgKey);
            $countFakeMsgBack           = 'countFakeMsgBack'.(OW_DEV_MODE ? '_dev:' : ':'). $userId;
            $countFakeMsg               = $this->redis->get($countFakeMsgBack);
            /*
            $opponentIds     = $this->redis->zrange('msg_user', 0, -1);
            $opponentIdIndex = mt_rand(0, count($opponentIds) - 1 );
            $opponentId      = $opponentIds[$opponentIdIndex];
            //*/
            $opponentId      = $this->getRandomUserIdByRedisKey('usersReceiveFakeMsgBack'.(OW_DEV_MODE ? '_dev:' : ':') . $userId);
            if ( $opponentId != null ) continue;

            if ( !in_array($opponentId, $usersReceiveFakeMsg) && ($num = mt_rand(0, 100)) < 60 && $countFakeMsg <= 5 )
            {
                $this->redis->incr($countFakeMsg);
                $this->redis->zadd('usersReceiveFakeMsg'.(OW_DEV_MODE ? '_dev:' : ':') . $userId, $opponentId, $opponentId);

                $this->redis->zrem('usersReceiveFakeMsgBack'.(OW_DEV_MODE ? '_dev:' : ':') . $userId, $opponentId);
                $this->redis->zrem('usersReceiveFakeMsgBack'.(OW_DEV_MODE ? '_dev:' : ':') . $opponentId, $userId); /// ?????

                // send fake massage
                $idMessageTemplateWasSend   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplateIdsFromMessageBySenderAndRecipient($userId);
                $aMessageTemplateType4      = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplatesByTypeIgnoreIds(array(4), $idMessageTemplateWasSend);
                if ( count($aMessageTemplateType4) == 0 ) continue;

                $messTemplateIndex          = mt_rand(0, count($aMessageTemplateType4) - 1);
                $templateSelected           = $aMessageTemplateType4[$messTemplateIndex];

                $pushTime = new DateTime('now', new \DateTimeZone('UTC'));
                $mandrill_time = clone $pushTime;
                $pushTime->add(new DateInterval('PT' . mt_rand(3*60, 5*60) . 'S'));

                \OW::getEventManager()->call('mailbox.post_message',
                    array(
                        'mode'                  => 'chat',
                        'userId'                => $userId,
                        'opponentId'            => $opponentId,
                        'text'                  => $templateSelected->text,
                        'is_cron'               => true,
                        'is_auto'               => true,
                        'message_template_id'   => $templateSelected->id,
                        'timeStamp'             => $mandrill_time->getTimestamp(),
                    )
                );

                // send push for fakeMsgBack
                $parse->sendPush($userId, function ($sender, $user) use ($parse, $mandrill_time) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name'     => ucfirst(mb_strtolower($user->username)),
                            'sender_name'   => ucfirst(mb_strtolower($sender->username)),
                            'message' =>  sprintf("<h2 style='font-weight:normal !important;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version), //'link_url'      => 'https://bnc.lt/m/DizNpFWdyp',
                            'link_title'    => 'Read Message'
                        ],
                        "Somebody wrote you a new message.", $mandrill_time);
                    return sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She');
                }, "message", $opponentId, $pushTime, false);
            }
        }
    }

    private function getRandomUserIdByRedisKey ($redis_key)
    {
        $userIds = $this->redis->zrange($redis_key, 0, -1);

        if (($count = count($userIds)) > 0)
        {
            $userIdsIndex = mt_rand(0, $count - 1);
            return $userIds[$userIdsIndex];
        }
        return null;
    }

    public function resetCounterFakeMsgBack()
    {
        // run every 15 minutes!!!!!
        $userIds = $this->redis->zrange('wink_user'.(OW_DEV_MODE ? '_dev' : ''), 0, -1);
        $users = [];
        foreach ( $userIds as $userId )
        {
            $countFakeMsgBack           = 'countFakeMsgBack'.(OW_DEV_MODE ? '_dev:' : ':'). $userId;
            $countFakeMsg               = $this->redis->get($countFakeMsgBack);
            if ( $countFakeMsg >= 5 )
            {
                $users[] = $userId;
            }
        }

        $resObjects = \BOL_UserService::getInstance()->getUsersForResetCounter($users);
        foreach ( $resObjects as $user )
        {
            $next_time = /*mt_rand(3, 5)*60 + */ $user->timeStampResetCounter + 2 * 3600;
            $countFakeMsgBack           = 'countFakeMsgBack'.(OW_DEV_MODE ? '_dev:' : ':'). $user->id;

            \BOL_UserService::getInstance()->updateTimeStampResetCounter([$user->id], $next_time);
            $this->redis->del($countFakeMsgBack);
        }
    }

    public function sendPushForUnreviewWinks()
    {
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $userIds = \BOL_UserService::getInstance()->sendPushForUnreviewWinks();
        echo date('Y-m-d H:i:s') . ' winks users count: ' . count($userIds) . PHP_EOL;
        $i=0;

        foreach ($userIds as $userId=>$data)
        {
            //$user = \BOL_UserService::getInstance()->findUserById($userId);
            //$userTimeZone = $user->newTimezone ? $user->newTimezone : ($user->timezone ?  : 'America/New_York');
            $userTimeZone = !empty($data['newTimezone']) ? $data['newTimezone'] : (!empty($data['timezone']) ? $data['timezone'] : 'America/New_York');
            $dateTime = new \DateTime();
            $dateTime->setTimezone(new \DateTimeZone($userTimeZone));

            $dateTime18 = clone $dateTime;
            $dateTime18->setTime(18, 0, 0);
            
            $dateTime22 = clone $dateTime;
            $dateTime22->setTime(22, 0, 0);

            $counter = (int)$data['count']; //OW::getEventManager()->call('winks.count_winked_me', array('userId' => $userId));
            if ( $counter >= 3 && $dateTime18 < $dateTime && $dateTime22 > $dateTime)
            {
                $i++;
                \BOL_UserService::getInstance()->updateUserSendWink($userId, true);
                $parse->sendPush($userId, function ($sender, $user) use ($counter) {
                    //return 'You have ' . $counter . ' unreview "winks"';
                    return "You got {$counter} winks. Find out who sent them right now!";
                }, 'WINK', $userId);
            }
        }
        echo 'send winks count: ' . $i++ . PHP_EOL;
    }

    public function sendPushForUnreadMsg()
    {
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $userIds = \BOL_UserService::getInstance()->sendPushForUnreadMsg();
        echo date('Y-m-d H:i:s') . ' msg users count: ' . count($userIds) . PHP_EOL;
        $i = 0;
        
        foreach ($userIds as $userId=>$data)
        {
            $user = \BOL_UserService::getInstance()->findUserById($userId);
            //$userTimeZone = $user->newTimezone ? $user->newTimezone : ($user->timezone ?  : 'America/New_York');
            $userTimeZone = !empty($data['newTimezone']) ? $data['newTimezone'] : (!empty($data['timezone']) ? $data['timezone'] : 'America/New_York');

            $dateTime = new \DateTime();
            $dateTime->setTimezone(new \DateTimeZone($userTimeZone));

            $dateTime18 = clone $dateTime;
            $dateTime18->setTime(18, 0, 0);
            
            $dateTime22 = clone $dateTime;
            $dateTime22->setTime(22, 0, 0);

            $counter = (int)$data['count']; //OW::getEventManager()->call('winks.count_winked_me', array('userId' => $userId));
            if ( $counter >= 3 && $dateTime18 < $dateTime && $dateTime22 > $dateTime )
            {
                $i++;
                \BOL_UserService::getInstance()->updateUserSendMsg($userId, true);
                //$parse->sendPush($userId, 'You have ' . $count . ' unread messages');
                $parse->sendPush($userId, function ($sender, $user) use ($counter) {
                    return "You have more than 3 unread messages. You can read and write the answer right now.";
                }, 'message', $userId);
            }
        }
        echo 'send msg count: ' . $i++ . PHP_EOL;
    }

    public function sendMailTest()
    { 
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        
        $sender = \BOL_UserService::getInstance()->findUserById(731774);
        $user = \BOL_UserService::getInstance()->findUserById(710341);
        $pushTime = new \DateTime('now', new \DateTimeZone('UTC'));
        $mandrill_time = clone $pushTime;

        //$parse->sendEmail('rabotoguru@gmail.com', $sender,
        $parse->sendEmail('artemkastanello@gmail.com', $sender,
        array(
            'user_name'     => ucfirst(mb_strtolower($user->username)),
            'sender_name'   => ucfirst(mb_strtolower($sender->username)),
            'message' =>  sprintf("<h2 style='font-weight:normal !important;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
            //'link_url'      => 'https://bnc.lt/m/DizNpFWdyp',
            //'link_url'      => 'https://bnc.lt/IAah/mPAaVNssyA',
            'link_url' => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version), //'link_url'      => 'https://bnc.lt/m/PA0u6rBeyp',
            'link_title'    => 'Read Message'
        ),
        "Somebody wrote you a new message.", $mandrill_time);
    }
    /*
    public function getFirstMessageNewDev($user)
    {
        try {
        $viewerId = $this->redis->zrange('msg_matches:' . $user->id, 0, 1);

        if (is_array($viewerId)) $viewerId = $viewerId[0];

        if ($viewerId == 0 || $viewerId == null) return;

        $sender = \BOL_UserService::getInstance()->findUserById($viewerId);

        $idMessageTemplateWasSend   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplateIdsFromMessageBySenderAndRecipient($viewerId);
        $aMessageTemplateType1or2   = \MAILBOX_BOL_MessageTemplateDao::getInstance()->getMessageTemplatesByTypeIgnoreIds(array(2), $idMessageTemplateWasSend);
        $messTemplateIndex          = mt_rand(0, count($aMessageTemplateType1or2)-1);
        $templateSelected           = $aMessageTemplateType1or2[$messTemplateIndex];

        //$time_view = time() + 30;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone([$user->id]);
        $time_view = new \DateTime('now', new \DateTimeZone('UTC'));
        $randomSecond = mt_rand(35*60, 50*60); // 15-25 min
        $time_view->add(new \DateInterval('PT'. $randomSecond .'S'));

        $user =  \BOL_UserService::getInstance()->findByIdWithoutCache($user->id);

        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $parse->sendEmail($user->email, $sender,
            [
                'user_name'     => ucfirst(mb_strtolower($user->username)),
                'sender_name'   => ucfirst(mb_strtolower($sender->username)),
                'message'       => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s sent you a message. %s is online. Message back!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
                //'link_url'      => 'https://bnc.lt/m/DizNpFWdyp',
                'link_url'      => \OW_Config::getInstance()->getValue('branch_email_got_message', $user->api_version),
                'link_title'    => 'Read Message'
            ],
            "Somebody wrote you a new message.", $time_view);

        $this->redis->zrem('msg_matches:' . $user->id, $viewerId);
        $this->redis->zrem('fav_matches:'.$user->id, $viewerId);
        $this->redis->zrem('wink_matches:'.$user->id, $viewerId);
        $this->redis->zrem('speedmatch_matches:'.$user->id, $viewerId);

        $time_view->setTimezone( new \DateTimeZone( $usersTimezone[$user->id] ) );
        \OW::getEventManager()->call('mailbox.post_message',
            array(
                'mode'                  => 'chat',
                'userId'                => $viewerId,
                'opponentId'            => $user->id,
                'text'                  => $templateSelected->text,
                'is_cron'               => true,
                'is_auto'               => true,
                'message_template_id'   => $templateSelected->id,
                'timeStamp'             => $time_view->getTimestamp(),
            )
        );

        return [
            'timestamp' => $time_view->getTimestamp(),
            'message'   => sprintf("Just now %s sent you a message. %s is online. Message back!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'He' : 'She'),
            'idUser'    => $viewerId,
            'screen'    => 'MESSAGE'
            ];
        }
        catch (Exception $ex) {
            trigger_error('sendfirstmsg' . $ex->getMessage());
            return [];
        }
    }
    
    public function getFirstWinkNewDev($user)
    {
        $viewerId = $this->redis->zrange('wink_matches:' . $user->id, 0, 1);

        if (is_array($viewerId)) $viewerId = $viewerId[0];

        if ($viewerId == 0 || $viewerId == null) return;

        $sender = \BOL_UserService::getInstance()->findUserById($viewerId);

        //$time_view = time() + 105;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone([$user->id]);
        $time_view = new \DateTime('now', new \DateTimeZone('UTC'));
        $randomSecond = mt_rand(1620, 2220);
        $time_view->add(new \DateInterval('PT' . $randomSecond . 'S'));

        $this->redis->zrem('msg_matches:' . $user->id, $viewerId);
        $this->redis->zrem('fav_matches:' . $user->id, $viewerId);
        $this->redis->zrem('wink_matches:' . $user->id, $viewerId);
        $this->redis->zrem('speedmatch_matches:' . $user->id, $viewerId);

        try {
            $service = \WINKS_BOL_Service::getInstance();
            $user =  \BOL_UserService::getInstance()->findByIdWithoutCache($user->id);

            $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
            $parse->sendEmail($user->email, $sender,
            [
                'user_name' => ucfirst(mb_strtolower($user->username)),
                'sender_name' => ucfirst(mb_strtolower($sender->username)),
                'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s winked at you. Wink back while %s online!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she'),
                //'link_url' => 'https://bnc.lt/m/eArjJ1qeyp'
                'link_url' => \OW_Config::getInstance()->getValue('branch_email_winked_me', $user->api_version),
            ],
            "Somebody has winked at you", $time_view);
        } 
        catch (Exception $ex) {
            var_dump($ex->getMessage() . ' errrrrror');
        }
        
        $time_view->setTimezone( new \DateTimeZone($usersTimezone[$user->id]) );
        $service->sendWink($viewerId, $user->id, true, $time_view, true);


        return [
            'timestamp' => $time_view->getTimestamp(),
            'message'   => sprintf("Just now %s winked at you. Wink back while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her'),
            'idUser'    => $viewerId,
            'screen'    => 'WINK'
        ];
    }

    public function addToFavoriteDev(array $users = null, $is_now = false)
    {
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('fav_user', 0, -1));

        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $i = 0;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);
        foreach ($notify as $uid)
        {
            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 40*60, 48*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $pushTime == null ) continue;
                $count_key = 'fav_sent:'.$uid;

                if ( !$is_now )
                {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $count = $this->redis->get($count_key);
                        if ( $count >= 2 ) continue 2; // 3 winks alredy sent for today
                    }
                }
                $viewerId = $this->redis->zrange('fav_matches:'.$uid, 0, 1);
             
                if (is_array($viewerId)) $viewerId = $viewerId[0];
             
                if ($viewerId == 0 || $viewerId === null) continue 2;

                if ( $viewerId == 743373 ) {
                    $this->redis->zrem('wink_matches:' . $uid, $viewerId);
                    $this->redis->zrem('fav_matches:' . $uid, $viewerId);
                    $this->redis->zrem('msg_matches:' . $uid, $viewerId);
                    $this->redis->zrem('speedmatch_matches:'.$uid, $viewerId);

                    $viewerId = $this->redis->zrange('fav_matches:'.$uid, 0, 1);

                    if (is_array($viewerId)) $viewerId = $viewerId[0];

                    if ($viewerId == 0 || $viewerId === null) { 
                        continue 2;
                    }
                }

                $pushTime->setTimezone(new \DateTimeZone('UTC')); // convert user timesone to UTC time zone
                $mandrill_time = clone $pushTime;

                \OW::getEventManager()->call("bookmarks.mark", array(
                    "userId"         => $viewerId,
                    "markUserId"     => $uid,
                    "timeStampVisit" => $mandrill_time->getTimestamp(),
                    "isautomark"     => true,
                    "iscron"         => true
                ));
             
                $this->redis->zrem('fav_matches:'.$uid, $viewerId);
                $this->redis->zrem('wink_matches:'.$uid, $viewerId);
                $this->redis->zrem('msg_matches:'.$uid, $viewerId);
                $this->redis->zrem('speedmatch_matches:'.$uid, $viewerId);
                if (empty($users) )
                    echo sprintf("%d fav to %d\n", $viewerId, $uid);

                if ( !$is_now ) {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $this->redis->incr($count_key);
                    }
                }

                $parse->sendPush($uid, function ($sender, $user) use ($parse, $mandrill_time) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' =>  sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>Just now %s added you to Favorites. Send %s a message while %s online!</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8')), $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she'),
                            //'link_url' => 'https://bnc.lt/m/sy7xzbJeyp'
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_faved_me', $user->api_version),
                        ],
                        "Somebody added you to favorites", $mandrill_time);
             
                    return sprintf("Just now %s added you to Favorites. Send %s a message while %s online!", ucfirst(mb_strtolower($sender->username, 'UTF-8')),$sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'him' : 'her', $sender->accountType == '808aa8ca354f51c5a3868dad5298cd72' ? 'he' : 'she');
                }, "FAVED", $viewerId, $pushTime, false);
            }
            $i++;
        }
        if (empty($users) )
            echo "sent count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
    }
    
    public function sendMessageDev(array $users = null, $is_now = false)
    {
        $notify = $users ? $users : array_map('intval', $this->redis->zrange('msg_user', 0, -1));
        $service = \WINKS_BOL_Service::getInstance();
        $parse = \SKADATEIOS_ACLASS_Parse::getInstance();
        $i = 0;
        $usersTimezone = \BOL_UserService::getInstance()->getUsersTimezone($notify);
        foreach ($notify as $uid)
        {
            $user_time_zone = 'America/New_York';
            if (isset($usersTimezone[$uid]))
            {
                $user_time_zone = empty($usersTimezone[$uid]) ? $user_time_zone : $usersTimezone[$uid];
            }

            $pushTimes = $this->getUserUTCFuturePushTimes($user_time_zone, !empty($users), $is_now, 50*60, 60*60 );

            foreach($pushTimes as $key => $pushTime)
            {
                if ( $pushTime == null ) continue;

                $count_key = 'msg_sent:' . $uid;

                if ( !$is_now )
                {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $count = $this->redis->get($count_key);
                        if ( $count >= 2 ) continue 2; // 3 winks alredy sent for today
                    }
                }

                $viewerId = $this->redis->zrange('msg_matches:' . $uid, 0, 1);
                if ( is_array($viewerId) ) $viewerId = $viewerId[0];

                if ( $viewerId == 0 || $viewerId === null ) continue 2;
                
                if ( $viewerId == 743373 ) {
                    $this->redis->zrem('wink_matches:' . $uid, $viewerId);
                    $this->redis->zrem('fav_matches:' . $uid, $viewerId);
                    $this->redis->zrem('msg_matches:' . $uid, $viewerId);
                    $this->redis->zrem('speedmatch_matches:'.$uid, $viewerId);

                    $viewerId = $this->redis->zrange('msg_matches:'.$uid, 0, 1);

                    if (is_array($viewerId)) $viewerId = $viewerId[0];

                    if ($viewerId == 0 || $viewerId === null) { 
                        continue 2;
                    }
                }

                $pushTime->setTimezone(new \DateTimeZone('UTC')); // convert user timesone to UTC time zone
                $mandrill_time = clone $pushTime;

                $event = new \OW_Event('guests.track_visit', array(
                    'userId' => $uid,
                    'guestId' => $viewerId,
                    'isautovisit' => true,
                    'timeStampVisit' => $mandrill_time->getTimestamp(),
                    "iscron" => true
                ));
                OW::getEventManager()->trigger($event);

                $this->redis->zrem('msg_matches:' . $uid, $viewerId);
                $this->redis->zrem('fav_matches:' . $uid, $viewerId);
                $this->redis->zrem('wink_matches:' . $uid, $viewerId);
                $this->redis->zrem('speedmatch_matches:' . $uid, $viewerId);
                if ( empty($users) )
                    echo sprintf("%d sends msg to %d\n", $viewerId, $uid);

                if ( !$is_now ) {
                    if ( $key != 'morning' && $key != 'user_datetime_tomorrow_10') {
                        $this->redis->incr($count_key);
                    }
                }

                $parse->sendPush($uid, function ( $sender, $user ) use ( $parse, $mandrill_time ) {
                    $parse->sendEmail($user->email, $sender,
                        [
                            'user_name' => ucfirst(mb_strtolower($user->username)),
                            'sender_name' => ucfirst(mb_strtolower($sender->username)),
                            'message' => sprintf("<h2 style='font-weight:normal !important;font-size: 18px;margin-top: 0px;'>%s checked you out! Find out more.</h2>", ucfirst(mb_strtolower($sender->username, 'UTF-8'))),
                            //'link_url' => 'https://bnc.lt/m/DizNpFWdyp'
                            'link_url' => \OW_Config::getInstance()->getValue('branch_email_viewed_me', $user->api_version),
                        ],
                        "Somebody viewed your profile", $mandrill_time);

                    return sprintf("%s checked you out! Find out more.", ucfirst(mb_strtolower($sender->username, 'UTF-8')));
                }, "VIEW", $viewerId, $pushTime, false);
            }
            $i++;
        }
        if (empty($users) )
            echo "sent count: ".$i." end: ".date('Y-m-d H:i:s')."\n";
    }
    //*/
    public function updateFields() {

        $pm = \OW::getPluginManager();

        self::$basicNames[] = OW::getConfig()->getValue('base', 'display_name_question');
        self::$basicNames[] = 'birthdate';
        self::$basicNames[] = 'sex';
        if ( $pm->isPluginActive('googlelocation') )
        {
            self::$basicNames[] = 'googlemap_location';
        }

        self::$searchFilledNames[] = 'sex';
        if ( $pm->isPluginActive('googlelocation') )
        {
            self::$searchFilledNames[] = 'googlemap_location';
        }
        self::$searchFilledNames[] = 'relationship';
        self::$searchFilledNames[] = 'birthdate';

        self::$searchBasicNames[] = 'sex';
        self::$searchBasicNames[] = 'match_sex';
        if ( $pm->isPluginActive('googlelocation') )
        {
            self::$searchBasicNames[] = 'googlemap_location';
        }
        self::$searchBasicNames[] = 'relationship';
        self::$searchBasicNames[] = 'birthdate';



        //$userId = 774283;
        $userDao = \BOL_UserDao::getInstance();
        $usersCount = $userDao->countModerated(1);

        echo date('Y-m-d H:i:s') . PHP_EOL;
        $users = $userDao->getModerated(1, $usersCount);

        var_dump( $usersCount );
        foreach ( $users as $u) {
            $userId = $u['id'];

            $service         = \SKADATEIOS_ABOL_Service::getInstance();
            $questionService = \BOL_QuestionService::getInstance();
            $userService     = \BOL_UserService::getInstance();
            $user            = $userService->findUserById($userId);
         
            $accountType     = $user->accountType;
         
            // ==================
                    // edit questions
            $editQuestionList = $questionService->findEditQuestionsForAccountType($accountType);
            $editNames = array();
         
            foreach ( $editQuestionList as $editQuestion )
            {
                $editNames[] = $editQuestion['name'];
            }
         
            $editQuestionList = $this->getUserEditQuestions($userId, $editNames);
            $editOptions = $questionService->findQuestionsValuesByQuestionNameList($editNames);
            $editData = $questionService->getQuestionData(array($userId), $editNames);
            $editData = !empty($editData[$userId]) ? $editData[$userId] : array();
         
            $editSections = array();
         
            foreach ( $editQuestionList['questions'] as $sectionName => $section )
            {
                if ( $sectionName == 'location' )
                {
                    continue;
                }
         
                $order = 0;
                foreach ( $sortedSections as $sorted )
                {
                    if ( $sorted->name == $sectionName )
                    {
                        $order = $sorted->sortOrder;
                    }
                }
                $editSections[] = array('order' => $order, 'name' => $sectionName, 'label' => $questionService->getSectionLang($sectionName));
            }
         
            usort($editSections, array('SKADATEIOS_ACTRL_User', 'sortSectionsAsc'));
         
            $editQuestions = array();
            $editBasic = array();
         
            foreach ( $editQuestionList['questions'] as $sectName => $section )
            {
                $sectionQuestions = array();            
                foreach ( $section as $question )
                {
                    $data = $editQuestionList['data'][$userId];
                    $name = $question['name'];
         
                    $values = "";
                    if ( !empty($data[$name]) )
                    {
                        if ( is_array($data[$name]) )
                        {
                            $values = array();
                            foreach ( $data[$name] as $val )
                            {
                                $values[] = strip_tags($val);
                            }
                        }
                        else
                        {
                            $values = strip_tags($data[$name]);
                        }
                    }
         
                    $q = array(
                        'id' => $question['id'],
                        'name' => $name,
                        'label' => $questionService->getQuestionLang($name),
                        //'value' => $name == 'field_2adbb972827885a0e7c794f76ae39908' ? (isset($values[0]) ? $values[0] :'Not Set') :  $values,
                        'value' =>$values ? $values : null,
                        //'rawValue' => $name == 'field_2adbb972827885a0e7c794f76ae39908' ? null : (!(empty($editData[$name])) ? $editData[$name] : null),
                        'rawValue' => !(empty($editData[$name])) ? $editData[$name] : null,
                        'section' => $sectName,
                        'custom' => json_decode($question['custom'], true),
                        'presentation' => $name == 'googlemap_location' ? $name : $question['presentation'],
                        'options' => self::formatOptionsForQuestion($name, $editOptions)
                    );
         
                    if ( in_array($name, self::$basicNames) )
                    {
                        $editBasic[] = $q;
                    }
                    else
                    {
                        $sectionQuestions[] = $q;
                    }
                }
                
                $editQuestions[$sectName] = array_reverse($sectionQuestions);
            }
         
            
         
            $viewQuestionList = array();
            $editQuestionList = array();
            
            $viewSectionList = array();
            $editSectionList = array();
         
            foreach ( $viewSections as $section )
            {
                unset($section['index']);
                $viewSectionList[] = $section;
                
                $viewQuestionList[] = empty($viewQuestions[$section["name"]])
                        ? array()
                        : $viewQuestions[$section["name"]];
            }
         
            foreach ( $editSections as $section )
            {
                unset($section['index']);
                $editSectionList[] = $section;
                
                $editQuestionList[] = empty($editQuestions[$section["name"]])
                        ? array()
                        : $editQuestions[$section["name"]];
            }
         
         
            // view fields
                    $viewQuestionList = $questionService->findViewQuestionsForAccountType($accountType);
            $viewNames = array();
         
            foreach ( $viewQuestionList as $viewQuestion )
            {
                $viewNames[] = $viewQuestion['name'];
            }
         
            $viewQuestionList = \BOL_UserService::getInstance()->getUserViewQuestions($userId, false, $viewNames);
            $viewSections = array();
            $sortedSections = $questionService->findSortedSectionList();
         
            foreach ( $viewQuestionList['questions'] as $sectionName => $section )
            {
                if ( $sectionName == 'location' )
                {
                    continue;
                }
         
                $order = 0;
                foreach ( $sortedSections as $sorted )
                {
                    if ( $sorted->name == $sectionName )
                    {
                        $order = $sorted->sortOrder;
                    }
                }
                $viewSections[] = array('order' => $order, 'name' => $sectionName, 'label' => $questionService->getSectionLang($sectionName));
            }
         
            usort($viewSections, array('SKADATEIOS_ACTRL_User', 'sortSectionsAsc'));
         
            $viewQuestions = array();
            $viewBasic = array();
            $data = $viewQuestionList['data'][$userId];
         
            foreach ( $viewQuestionList['questions'] as $sectName => $section )
            {
                $sectionQuestions = array();
                
                foreach ( $section as $question )
                {
                    $name = $question['name'];
         
                    if ( is_array($data[$name]) )
                    {
                        $values = array();
                        foreach ( $data[$name] as $val )
                        {
                            $values[] = strip_tags($val);
                        }
                    }
                    else
                    {
                        $values = strip_tags($data[$name]);
                    }
         
                    if ( in_array($name, self::$basicNames) )
                    {
                        if ( $name == 'sex' )
                        {
                            $v = array_values($values);
                            $viewBasic[$name] = array_shift($v);
                        }
                        else
                        {
                            $viewBasic[$name] = $values;
                        }
                    }
                    else
                    {
                        $sectionQuestions[$name] = array(
                            'id' => $question['id'],
                            'name' => $name,
                            'label' => $questionService->getQuestionLang($name),
                            //'value' => $name == 'field_2adbb972827885a0e7c794f76ae39908' ? (isset($values[0]) ? $values[0] : 'Not Set' ): (!empty($values) ? $values : 'No answer'),
                            'value' => $values ? $values : null,
                            'section' => $sectName,
                            'presentation' => $name == 'googlemap_location' ? $name : $question['presentation']
                        );
                    }
                }
                $_sectionQuestions = array();
                foreach ($viewNames as $fieldName)
                {
                    if (array_key_exists($fieldName, $sectionQuestions))
                        $_sectionQuestions[] = $sectionQuestions[$fieldName];
                }
                //$viewQuestions[$sectName] = array_reverse($sectionQuestions);
                $viewQuestions[$sectName] = $_sectionQuestions;
            }
         
            //var_dump( /json_encode($viewQuestions, 256) );
         
            //var_dump( json_encode($editQuestionList, 256) );
         
            $emptyFilelds = [];
         
            foreach( $editQuestionList as $section ) {
         
                foreach( $section as $_field ) {
         
                    $selectedValue = 0;
                    if ( ($_field['name'] == 'field_2adbb972827885a0e7c794f76ae39908') && ( empty($_field['rawValue']) || is_null( $_field['rawValue']) ) ) {
                        // relations
                        if (mt_rand(0, 75) < 100 ) {
                            $selectedValue = 1;
                        }
                        else {
                            $selectedValue = 2;
                        }
                    }
                    else if ( ($_field['name'] == 'field_db4116ff8e3e94e920a256167a040431') && (empty($_field['rawValue']) || is_null( $_field['rawValue'])) ) {
                        // income
         
                        if ( mt_rand(0, 50) < 100 ) {
         
                            $count = $_field['options']['count'] - 1;
                            $values = $_field['options']['values'];
         
                            $selectedValue = $values[mt_rand(0, $count)]['value'];
                        }
         
                    }
                    else if ( ($_field['name'] == 'field_15c0a2bb49bf04bc1251d9a6b96b5644') &&
                        ( empty($_field['rawValue']) || is_null( $_field['rawValue'])) ) { // have a hobby ?
         
                        $hobbyValuesForAccountType = [
                            // man
                            '808aa8ca354f51c5a3868dad5298cd72' => [
                                4,8,16,128,256,1024,2048,4096,65536,131072,262144,524288,1048576,2097152,8388608,33554432,67108864
                            ],
                            // woman
                            '8cc28eaddb382d7c6a94aeea9ec029fb' => [
                                1,2,16,64,128,512,2048,4096,8192,16384,32768,524288,2097152,16777216,67108864
                            ]
                        ];
         
                        $valuesForField = $hobbyValuesForAccountType[$accountType];
                        
                        $i=0;
                        $k = mt_rand(2, 5);
                         $count = count($valuesForField);
                        if ( $k > $count ) {
                            $k = mt_rand(1, $count );
                        }
         
                        do {
         
                            $count = count($valuesForField)-1;
         
                            $y = mt_rand(1, $count );
         
                            $selectedValue += $valuesForField[$y];
         
                            unset($valuesForField[$y]);
         
                            $i++;
                        } while( $i < $k );
                    }
                    else if ( $_field['name'] == 'field_6a72f8041256010bccf59f5397e02b8e' && 
                        (( empty($_field['rawValue']) ) || is_null( $_field['rawValue'])) ) { // kinds of sport ?
         
                        $kindsOfSportValuesForAccountType = [
                            // man
                            '808aa8ca354f51c5a3868dad5298cd72' => [
                                2,4,8,16,32,64,256,512,1024,2048,4096,8192,16384,32768,65536,131072,262144,524288,1048576,4194304
                            ],
                            // woman
                            '8cc28eaddb382d7c6a94aeea9ec029fb' => [
                                1,64,128,8192,65536,131072,262144,524288,2097152,4194304
                            ]
                        ];
         
                        $valuesForField = $kindsOfSportValuesForAccountType[$accountType];
                        $count = count($valuesForField);
                        $i=0;
                        $k = mt_rand(2, 5);
                        if ( $k > $count ) {
                            $k = mt_rand(1, $count );
                        }
         
                        do {
         
                            $count = count($valuesForField)-1;
         
                            $y = mt_rand(1, $count );
         
                            $selectedValue += $valuesForField[$y];
         
                            unset($valuesForField[$y]);
         
                            $i++;
                        } while( $i < $k );
         
                    }
                    else if ( $_field['name'] == 'field_7e9dd8d5d39ceab079b02bac7c2c1b04' && 
                        ( empty($_field['rawValue']) || is_null( $_field['rawValue']) )  ) { // do for a living ?
         
                        $doForALivingValuesForAccountType = [
                            // man
                            '808aa8ca354f51c5a3868dad5298cd72' => [
                                1,4,8,128,256,512,2048,8192,16384,32768,65536,131072,262144,524288,2097152
                            ],
                            // woman
                            '8cc28eaddb382d7c6a94aeea9ec029fb' => [
                                1,2,4,8,16,32,64,128,256,512,2048,4096,16384,65536,262144,1048576
                            ]
                        ];
         
                        $valuesForField = $doForALivingValuesForAccountType[$accountType];
                        $count = count($valuesForField)-1;
         
                        $y = mt_rand(0, $count );
                        $val = $valuesForField[$y];
                        $selectedValue += $val;
         
         
                    }
                    else if ( empty($_field['rawValue']) || is_null($_field['rawValue']) ) {
                        $emptyFilelds[$_field['name']] = $_field;
         
                        if ( $_field['presentation'] == 'radio' ) {
                            $count = $_field['options']['count'] - 1;
                            $values = $_field['options']['values'];
         
                            $selectedValue = $values[mt_rand(0, $count)]['value'];
                        }
                    }
                        /*
                        else if ( $_field['presentation'] == 'multicheckbox') {
                            $selectedValue = 0;
                            $count = $_field['options']['count'];
                            $values = $_field['options']['count'];
         
                            $tmpSelectedValues = [];
         
                            $i=0;
                            $k = mt_rand(2, 5); // count iteration for save
            
                            if ( $k > $count ) {
                               $k = mt_rand(1, $count);
                            }
         
                            do {
                                $index = mt_rand(0, $count-1);
                                if ( isset($values[$index]) ) {
                                    $selectedValue = $values[$index]['value'];
                                    if ( !in_array($selectedValue, $tmpSelectedValues) ) {
                                        $tmpSelectedValues[] = $selectedValue;
                                        $i++;
                                    }
                                }
         
                            }
                            while( $i < $k );
         
         
                        }
                        else if ( $_field['presentation'] == 'text') {
                        }
                        //*/
                    if ( $selectedValue != 0 ) {
                        $questionService->saveQuestionsData(array($_field['name'] => $selectedValue), $userId);
                    }
                }
            }
        }
        echo date('Y-m-d H:i:s');
        //var_dump( json_encode($editQuestionList, 256) );
        //var_dump( json_encode($emptyFilelds, 256) );

    }

    public function getUserEditQuestions( $userId, $questionNames )
    {
        $questionService = \BOL_QuestionService::getInstance();
        $language = \OW::getLanguage();

        $questions = $questionService->findQuestionByNameList($questionNames);
        foreach ( $questions as &$q )
        {
            $q = (array) $q;
        }

        $section = null;
        $questionArray = array();
        $questionNameList = array();

        foreach ( $questions as $sort => $question )
        {
            if ( $section !== $question['sectionName'] )
            {
                $section = $question['sectionName'];
            }

            $questions[$sort]['hidden'] = false;

            if ( !$questions[$sort]['onView'] )
            {
                $questions[$sort]['hidden'] = true;
            }

            $questionArray[$section][$sort] = $questions[$sort];
            $questionNameList[] = $questions[$sort]['name'];
        }

        $questionData = $questionService->getQuestionData(array($userId), $questionNameList);
        $questionLabelList = array();

        // add form fields
        foreach ( $questionArray as $sectionKey => $section )
        {
            foreach ( $section as $questionKey => $question )
            {
                $event = new OW_Event('base.questions_field_get_label', array(
                    'presentation' => $question['presentation'],
                    'fieldName' => $question['name'],
                    'configs' => $question['custom'],
                    'type' => 'view'
                ));

                OW::getEventManager()->trigger($event);

                $label = $event->getData();

                $questionLabelList[$question['name']] = !empty($label) ? $label : $questionService->getQuestionLang($question['name']);

                $event = new OW_Event('base.questions_field_get_value', array(
                    'presentation' => $question['presentation'],
                    'fieldName' => $question['name'],
                    'value' => empty($questionData[$userId][$question['name']]) ? null : $questionData[$userId][$question['name']],
                    'questionInfo' => $question,
                    'userId' => $userId
                ));

                OW::getEventManager()->trigger($event);

                $eventValue = $event->getData();

                if ( !empty($eventValue) )
                {
                    $questionData[$userId][$question['name']] = $eventValue;

                    continue;
                }

                if ( !empty($questionData[$userId][$question['name']]) )
                {
                    switch ( $question['presentation'] )
                    {
                        case \BOL_QuestionService::QUESTION_PRESENTATION_CHECKBOX:

                            if ( (int) $questionData[$userId][$question['name']] === 1 )
                            {
                                $questionData[$userId][$question['name']] = OW::getLanguage()->text('base', 'yes');
                            }

                            break;

                        case \BOL_QuestionService::QUESTION_PRESENTATION_DATE:

                            $format = OW::getConfig()->getValue('base', 'date_field_format');

                            $value = 0;

                            switch ( $question['type'] )
                            {
                                case \BOL_QuestionService::QUESTION_VALUE_TYPE_DATETIME:

                                    $date = \UTIL_DateTime::parseDate($questionData[$userId][$question['name']], UTIL_DateTime::MYSQL_DATETIME_DATE_FORMAT);

                                    if ( isset($date) )
                                    {
                                        $format = OW::getConfig()->getValue('base', 'date_field_format');
                                        $value = mktime(0, 0, 0, $date['month'], $date['day'], $date['year']);
                                    }

                                    break;

                                case \BOL_QuestionService::QUESTION_VALUE_TYPE_SELECT:

                                    $value = (int) $questionData[$userId][$question['name']];

                                    break;
                            }

                            if ( $format === 'dmy' )
                            {
                                $questionData[$userId][$question['name']] = date("d/m/Y", $value);
                            }
                            else
                            {
                                $questionData[$userId][$question['name']] = date("m/d/Y", $value);
                            }

                            break;

                        case \BOL_QuestionService::QUESTION_PRESENTATION_BIRTHDATE:

                            $date = \UTIL_DateTime::parseDate($questionData[$userId][$question['name']], \UTIL_DateTime::MYSQL_DATETIME_DATE_FORMAT);
                            $questionData[$userId][$question['name']] = \UTIL_DateTime::formatBirthdate($date['year'], $date['month'], $date['day']);

                            break;

                        case \BOL_QuestionService::QUESTION_PRESENTATION_AGE:

                            $date = \UTIL_DateTime::parseDate($questionData[$userId][$question['name']], \UTIL_DateTime::MYSQL_DATETIME_DATE_FORMAT);
                            $questionData[$userId][$question['name']] = \UTIL_DateTime::getAge($date['year'], $date['month'], $date['day']) . " " . $language->text('base', 'questions_age_year_old');

                            break;

                        case \BOL_QuestionService::QUESTION_PRESENTATION_RANGE:

                            $range = explode('-', $questionData[$userId][$question['name']]);
                            $questionData[$userId][$question['name']] = $language->text('base', 'form_element_from') . " " . $range[0] . " " . $language->text('base', 'form_element_to') . " " . $range[1];

                            break;

                        case \BOL_QuestionService::QUESTION_PRESENTATION_SELECT:
                        case \BOL_QuestionService::QUESTION_PRESENTATION_RADIO:
                        case \BOL_QuestionService::QUESTION_PRESENTATION_MULTICHECKBOX:

                            $value = "";
                            $multicheckboxValue = (int) $questionData[$userId][$question['name']];

                            $parentName = $question['name'];

                            if ( !empty($question['parent']) )
                            {
                                $parent = \BOL_QuestionService::getInstance()->findQuestionByName($question['parent']);

                                if ( !empty($parent) )
                                {
                                    $parentName = $parent->name;
                                }
                            }

                            $questionValues = \BOL_QuestionService::getInstance()->findQuestionValues($parentName);
                            $value = array();

                            foreach ( $questionValues as $val )
                            {
                                /* @var $val BOL_QuestionValue */
                                if ( ( (int) $val->value ) & $multicheckboxValue )
                                {
                                     $value[$val->value] = \BOL_QuestionService::getInstance()->getQuestionValueLang($val->questionName, $val->value);
                                }
                            }

                            if ( !empty($value) )
                            {
                                $questionData[$userId][$question['name']] = $value;
                            }

                            break;

                        case \BOL_QuestionService::QUESTION_PRESENTATION_URL:
                        case \BOL_QuestionService::QUESTION_PRESENTATION_TEXT:
                        case \BOL_QuestionService::QUESTION_PRESENTATION_TEXTAREA:
                            if ( !is_string($questionData[$userId][$question['name']]) )
                            {
                                break;
                            }

                            $value = trim($questionData[$userId][$question['name']]);

                            if ( strlen($value) > 0 )
                            {
                                $questionData[$userId][$question['name']] = \UTIL_HtmlTag::autoLink(nl2br($value));
                            }

                            break;

                        default:
                            unset($questionArray[$sectionKey][$questionKey]);
                    }
                }
            }

            if ( isset($questionArray[$sectionKey]) && count($questionArray[$sectionKey]) === 0 )
            {
                unset($questionArray[$sectionKey]);
            }
        }

        return array('questions' => $questionArray, 'data' => $questionData, 'labels' => $questionLabelList);
    }

    private static function formatOptionsForQuestion( $name, $allOptions )
    {
        $options = array();
        $questionService = \BOL_QuestionService::getInstance();

        if ( !empty($allOptions[$name]) )
        {
            $optionList = array();
            foreach ( $allOptions[$name]['values'] as $option )
            {
                $optionList[] = array(
                    'label' => $questionService->getQuestionValueLang($option->questionName, $option->value),
                    'value' => $option->value
                );
            }

            $allOptions[$name]['values'] = $optionList;
            $options = $allOptions[$name];
        }

        return $options;
    }

    public static function sortSectionsAsc( $el1, $el2 )
    {
        if ( $el1['order'] === $el2['order'] )
        {
            return 0;
        }

        return $el1['order'] > $el2['order'] ? 1 : -1;
    }

    private static $basicNames = array();
    private static $searchBasicNames = array();
    private static $searchFilledNames = array();
}
