<?php

include __DIR__ . '/vendor/autoload.php';

use Dating\Notifier;

define('MODERATION_MODE', true);
define('_OW_', true);
define('DS', DIRECTORY_SEPARATOR);
define('OW_DIR_ROOT', __DIR__ .'/../dating_skadate/' . DS);
define('OW_CRONE', true);

require_once(OW_DIR_ROOT . 'ow_includes' . DS . 'init.php');
@include OW_DIR_ROOT . 'ow_install' . DS . 'install.php';

$application = \OW::getApplication();

if ( OW_PROFILER_ENABLE || OW_DEV_MODE ) {
    \UTIL_Profiler::getInstance()->mark('before_app_init');
}

// We should define custom request uri to avoid errors during application initialization
$_SERVER['REQUEST_URI'] = '/';
$application->init();

if ( OW_PROFILER_ENABLE || OW_DEV_MODE ) {
    \UTIL_Profiler::getInstance()->mark('after_app_init');
}

$event = new \OW_Event(\OW_EventManager::ON_APPLICATION_INIT);

\OW::getEventManager()->trigger($event);


Notifier::getInstance()->new_users_with_token();
